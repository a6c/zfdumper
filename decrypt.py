#!/usr/bin/env python
#key='Encrypt01'
#key='Acxylf365jw'
#key='jwc01'
#zfsoft_zju

def decrypt(pwd, key='Encrypt01'):
    key = (key * (len(pwd) / len(key) + 1))[:len(pwd)]
    l = len(pwd)
    if l % 2 == 0:
        key = key[0:l / 2][::-1] + key[l / 2:l][::-1]

    pwd = map(ord, pwd)
    key = map(ord, key)

    rst = []
    for i, p in enumerate(pwd):
        a = key[i] ^ p
        if a < 32 or a > 126:
            rst.append(p)
        else:
            rst.append(a)
    if l % 2 == 0:
        rst = rst[0:l / 2][::-1] + rst[l / 2:l][::-1]
    return ''.join(map(chr, rst))


if __name__ == '__main__':
    pwd = raw_input()
    print decrypt(pwd)